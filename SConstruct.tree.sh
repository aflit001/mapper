#!/bin/bash
rm SConstruct.tree.dot  2>/dev/null
rm SConstruct.tree.png  2>/dev/null
rm SConstruct.tree.gif  2>/dev/null
rm SConstruct.tree.html 2>/dev/null

bash SConstruct.run -n --tree=prune,status $@ | grep -v '+-/' > SConstruct.tree.dat
#,derived

cat SConstruct.tree.dat | python SConstruct.graph.py $@

#dot -Tgif tree.dot > tree.gif
#dot -Tpng tree.dot > tree.png

# | tee tree.dot | dot   -Tgif > tree.gif
#./run -n --tree=status $@ | tee tree.dat
#cat tree.dat | ./graph.py | neato -Tpng > neato_rendering.png
#cat tree.dat | ./graph.py | fdp   -Tpng > fdp_rendering.png
#cat tree.dat | ./graph.py | dot   -Tgif > tree.gif
cat SConstruct.tree.dot  | /usr/bin/dot -x -Tpng > SConstruct.tree.png
#cat tree.dat | ./graph.py | circo -Tpng > circo_rendering.png
