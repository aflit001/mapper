($num,$den,$min,$max,$av,$var)=(0,0,10000000,0,0,0);
while ($cov=<STDIN>) {
    if ( $cov >= 1 ) {
        $num += $cov;
        if ( $cov > $max ) { $max = $cov; };
        if ( $cov < $min ) { $min = $cov; };
        if ( $den == 0   ) { $av=$cov; $var=0; }
        else {
                $av   = (($av*$den)+$cov)/($den+1);
                $var += ($cov-$av)**2;
                $dev  = sqrt($var/$den);
		if ( $den % 10000 == 0 ) {
                	print "$den\t$av\t$var\t$dev\n";
		}
        };
        $den++;
    }
}
$cov=$num/$den;
$dev=sqrt($var/$den);
$mx=$cov+(2*$dev);
print "Length        = $den\n";
print "Bases         = $num\n";
print "Mean Coverage = $cov\n";
print "Min  Coverage = $min\n";
print "Max  Coverage = $max\n";
print "Var  Coverage = $var\n";
print "Dev  Coverage = $dev\n";
print "MX   Coverage = $mx\n";

