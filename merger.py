#!/usr/bin/python
import os, sys
import glob
from pprint import pprint as pp


if len(sys.argv) < 4:
    print "no libs given"
    print sys.argv[0], "<IN CSV GROUP> <IN CSV LIB> <OUT FILE>"
    sys.exit(1)


CSV_GROUP  = sys.argv[1]
CSV_LIB    = sys.argv[2]
CSV_OUT    = sys.argv[3]

if (not os.path.exists( CSV_GROUP )) or ((CSV_LIB is not None) and (not os.path.exists(CSV_LIB))):
    if (not os.path.exists( CSV_GROUP )):
        print "csv group file %s does not exists" % CSV_GROUP

    if ((CSV_LIB is not None) and (not os.path.exists(CSV_LIB))):
        print "csv lib file %s does not exists" % CSV_LIB

    print sys.arv[0], "<IN CSV GROUP> <IN CSV LIB>"
    sys.exit(1)

if os.path.exists( CSV_OUT ):
    print "output file %s already exists" % CSV_OUT
    sys.exit( 1 )




def main():
    data   = {}
    groups = {}

    with open(CSV_LIB, 'r') as fhd:
        line_count = 0
        for line in fhd:
            line_count += 1
            if line_count == 1:
                continue

            if len(line.strip()) == 0 or line[0] == '#':
                continue

            #0             1             2              3         4       5          6            7            8              9                 10             11
            #library_name, project_name, organism_name, type    , paired, frag_size, frag_stddev, insert_size, insert_stddev, read_orientation, genomic_start, genomic_end
            #frags_PE_170, arcanum     , sa           , fragment, 1     , 170      , 17         ,            ,              , inward          , 0            ,           0

            cels   = line.split(',')
            print  "parsing line '%s'" % line.strip()
            gname  = cels[0].strip()
            fsize  = cels[5].strip()
            fstdd  = cels[6].strip()
            isize  = cels[7].strip()
            istdd  = cels[8].strip()

            if gname not in data:
                groups[gname] = {}

            gtype = ""
            gsize = 0
            gmin  = 0
            gmax  = 0
            gmax2 = 0

            if len(fsize) > 0:
                gtype = 'pe'
                gsize = int(fsize)
                gmin  = int(fsize) - int(fstdd)
                gmax  = int(fsize) + int(fstdd)
                gmax2 = int(fsize) + (3*int(fstdd))

            elif len(isize) > 0:
                gtype = 'mp'
                #data[gname]['type'] = 'pe'
                gsize = int(isize)
                gmin  = int(isize) - int(istdd)
                gmax  = int(isize) + int(istdd)
                gmax2 = int(isize) + (3*int(istdd))

            else:
                print line
                print "count not find size"
                sys.exit(1)

            groups[gname]['gtype'] = gtype
            groups[gname]['gsize'] = gsize
            groups[gname]['gmin' ] = gmin
            groups[gname]['gmax' ] = gmax
            groups[gname]['gmax2'] = gmax2



    with open(CSV_GROUP, 'r') as fhd:
        line_count = 0
        for line in fhd:
            line_count += 1
            if line_count == 1:
                continue

            if len(line.strip()) == 0 or line[0] == '#':
                continue

            #0
            #file_name                                             , group_name, library_name
            #120527_I247_FCC0U0NACXX_L7_TOMxitDGJDWAAPEI-93_*.fq.gz, MP_2000_1 , jumps_MP_2000

            cels   = line.split(',')
            print  "parsing line '%s'" % line.strip()
            fname  = cels[0].strip()
            gname  = cels[2].strip()
            fnames = glob.glob(fname)

            if len(fnames) == 0:
                print "error globing '%s'" % fname.strip()
                print " line '%s'"         % line.strip()
                sys.exit(1)

            cpref = os.path.basename( os.path.commonprefix(fnames) )
            cpref = cpref.rstrip("_.-")
            print cpref

            if len(fnames) % 2 != 0:
                print "oops. failed to glob"
                sys.exit(1)

            elif len(fnames) == 2:
                data[cpref] = { "file_names": [] }

                for ffname in fnames:
                    print ffname

                    if not os.path.exists(ffname) or not os.path.isfile(ffname):
                        print "file %s does not exists" % ffname
                        sys.exit(1)

                    datal = data[cpref]
                    datal["file_names"].append( ffname )

                    for key in groups[gname]:
                        datal[ key] = groups[gname][ key ]

            else:
                print "oops2. globed more than 2 files"
                sys.exit(1)
                for ffname in fnames:
                    print ffname

                    if not os.path.exists(ffname) or not os.path.isfile(ffname):
                        print "file %s does not exists" % ffname
                        sys.exit(1)

                    data[ffname] = {}

    #pp(data)

    with open(CSV_OUT, 'w') as ofh:
        outlines = []
        for gname in sorted( data ):
            ostr = gname+"\t"+"illumina"
            datal = data[gname]
            for field in ['gtype', 'gsize', 'gmin', 'gmax', 'file_names']:
                opart  = ""
                fieldl = datal[ field ]
                if isinstance(fieldl, list):
                    opart += "\t" + ";".join( fieldl )

                else:
                    opart += "\t" + str(fieldl)

                ostr += opart
            print ostr
            outlines.append( ostr + "\n" )
        ofh.write("#group name\tplatform\tlibrary name\tlibrary size\tlibrary size min\tlibrary size max\tfile names\n")
        ofh.writelines( outlines )

if __name__ == '__main__':
    main()
    print "\n\n"
    print 'finished'
