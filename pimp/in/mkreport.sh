#!/bin/bash

set -xeu

WID=800
HEI=600
NUM=9


F1=$1
F2=$2


if [[ ! -f "white.png" ]]; then
	convert -size ${WID}x${HEI} xc:white white.png
fi

function do_report {
    reportfolderOut=$1
    reportfolder=${reportfolderOut}_fastqc

    if [[ ! -f "$reportfolder/Images/kmer_profiles.png" ]]; then
        #kmer_profiles.png
        cp white.png $reportfolder/Images/kmer_profiles.png
    fi

    images=`find $reportfolder/Images -type f -name '*.png' | sort | tr '\n' ' '`
    #echo "    IMAGES $images"
    IMC=`find $reportfolder/Images -type f -name '*.png' | wc -l`

    if [[ "$IMC" -ne "$NUM" ]]; then
        echo "wrong number of images $IMC (should be $NUM)"
        exit 1
    fi

    if [[ -f "$reportfolderOut.png" ]]; then
        #rm $reportfolder.png
        echo -n
    fi

    WIDT=$(($WID*1   ))
    HEIT=$(($HEI*$NUM))
    CMD="montage -verbose -label '%f' -font Helvetica  -pointsize 14 -background 'white' -fill 'black' -define png:size=${WIDT}x${HEIT} -geometry ${WID}x${HEI}+2+2 -tile 1x$NUM -auto-orient $images $reportfolderOut.png"
    echo $CMD
    eval $CMD
}


N1=${F1%%.*}
N2=${F2%%.*}

O1=$N1.png
O2=$N2.png

do_report $N1
do_report $N2


LCN=`python -c "import os,sys; print os.path.commonprefix([sys.argv[1], sys.argv[2]])" $N1 $N2`.png
LCN=`basename $LCN`
WIDT=$(($WID*2   ))
HEIT=$(($HEI*$NUM))

CMD="montage -verbose -label '%f' -font Helvetica  -pointsize 14 -background 'white' -fill 'black' -define png:size=${WIDT}x${HEIT} -geometry ${WID}x${HEIT}+2+2 -tile 2x1 -auto-orient $O1 $O2 $LCN"
echo "     LCN $LCN"
echo "     width        $WID  height       $HEI  num $NUM images 2"
echo "     width total  $WIDT height total $HEIT"
#echo "     CMD $CMD"
eval $CMD
