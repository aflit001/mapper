INBAM1=/home/assembly/tomato150/dev150/assemblies/mapping/pimp/output/C3VVEACXX.bam.filtered.bam
RUNNAME=/home/assembly/tomato150/dev150/assemblies/mapping/pimp/output/C3VVEACXX.bam.filtered.bam
REF=heinz.fa


samtools rmdup $INBAM1 $RUNNAME.dedup.bam
samtools calmd -AEur   $RUNNAME.dedup.bam $REF | samtools mpileup -d 99999 -L 99999 -Dugf $REF - | bcftools view -bvcg - > $RUNNAME.dedup.bam.bcf
bcftools view          $RUNNAME.dedup.bam.bcf  | /usr/share/samtools/vcfutils.pl varFilter -W 5 -Q 20 -d 4 -D100         > $RUNNAME.dedup.bam.bcf.vcf


samtools mpileup $RUNNAME.dedup.bam | awk '{print $4}' | perl mean_coverage.pl

cat $RUNNAME.dedup.bam.bcf.vcf | \
	grep -v \# | \
	cut -f 8 | \
	cut -d ';' -f 1 | \
	grep -v INDEL | \
	cut -d '=' -f 2 | \
	perl -ne 'BEGIN{$v=0;$c=0;$min=10000000;$max=0;$av=0;$var=0;} END{ $a=$v/$c; $dev=sqrt($var/$c); $mx=$a+(2*$dev); print "$c\t$v\t$a\t$min\t$max\t$av\t$var\t$dev\t$mx\n"; } $c+=1; $v+=$_; if ($_ < $min) { $min = $_; }; if ($_ > $max) { $max = $_; }; if ($c==1) { $av = $_; $var=0; } else { $av=(($av*$c)+$_)/($c+1); $var+=($_-$av)**2; $dev=sqrt($var/$c); print "$av\t$var\t$dev\n";}' | \
	tee $RUNNAME.dedup.bam.bcf.vcf.stats


samtools index    $RUNNAME.dedup.bam
samtools depth    $RUNNAME.dedup.bam > $RUNNAME.dedup.bam.cov
samtools idxstats $RUNNAME.dedup.bam > $RUNNAME.dedup.bam.idxflags
samtools flagstat $RUNNAME.dedup.bam > $RUNNAME.dedup.bam.stats

