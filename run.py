#!/usr/bin/python
import os, sys
import subprocess
import tempfile
import shutil

dry_run      = False
dry_run      = True

do_merge     = True
#do_merge     = False

FRC          = False

in_csv_lib   = None
in_fasta     = None

LINKER       = '/home/assembly/dev_150/assemblies/mapping/sff_linker.fasta'
LINKER       = '/home/assembly/dev_150/assemblies/mapping/sff_linker.fasta.all'
DB_FOLDER    = 'db'
MAP_FOLDER   = 'map'
OUT_FOLDER   = 'output'
GENOME_SIZE  = 950000000
TMP_FOLDER   = '/run/shm'
FRC_EXE      = '/home/assembly/nobackup/metrics/FRC_align/src/FRC'
BWA_EXE      = '/usr/bin/bwa'
SAMTOOLS_EXE = 'samtools'


if len(sys.argv) < 3:
    print "no contig given"
    print sys.argv[0], "<IN FASTA> <IN CSV CONFIG>"
    sys.exit(1)

CONTIG     = sys.argv[1]
CSV_GROUP  = sys.argv[2]


if (not os.path.exists( CONTIG )) or (not os.path.exists( CSV_GROUP )):
    if (not os.path.exists( CONTIG )):
        print "contig file %s does not exists" % CONTIG

    if (not os.path.exists( CSV_GROUP )):
        print "csv group file %s does not exists" % CSV_GROUP

    print sys.arv[0], "<IN FASTA> <IN CSV GROUP>"
    sys.exit(1)

if not os.path.exists(DB_FOLDER):
    os.mkdir(DB_FOLDER)

if not os.path.exists(MAP_FOLDER):
    os.mkdir(MAP_FOLDER)

if not os.path.exists(OUT_FOLDER):
    os.mkdir(OUT_FOLDER)




def run_cmd( CMD , name ):
    if not dry_run:
        print "  running %s :: %s" % ( name , CMD )
        ret = subprocess.call(CMD, shell=True, executable='/bin/bash')
        if ret != 0:
            if ret < 0:
                print "error running %s. killed by signal %d\n" % (name, ret)
                sys.exit(ret)
            else:
                print "error running %s. error #%d\n" % (name, ret)
                sys.exit(ret)
        else:
            print "    running %s finished successfully\n" % ( name )

        return ret
    else:
        print "  DRY RUN:: skipping %s :: %s\n" % ( name , CMD )
        return 0


def main():
    datas   = {}

    with open(CSV_GROUP, 'r') as fhd:
        line_count = 0
        for line in fhd:
            line_count += 1
            if line_count == 1:
                continue

            line = line.strip()
            if len(line) == 0 or line[0] == '#':
                continue

            #0
            #file_name                                             , group_name, library_name
            #120527_I247_FCC0U0NACXX_L7_TOMxitDGJDWAAPEI-93_*.fq.gz, MP_2000_1 , jumps_MP_2000

            #120624_I232_FCC0U3HACXX_L5_TOMxitDGJDWAAPEI-93,mp,2000,1800,2200,/home/assembly/tomato150/denovo/arcanum/raw/illumina/matepair_2000/120624_I232_FCC0U3HACXX_L5_TOMxitDGJDWAAPEI-93_2.fq.gz;/home/assembly/tomato150/denovo/arcanum/raw/illumina/matepair_2000/120624_I232_FCC0U3HACXX_L5_TOMxitDGJDWAAPEI-93_1.fq.gz

            cels   = line.split('\t')
            print  "parsing line '%s'" % str(cels)
            if len(cels) != 7:
                print "wrong number of columns"
                sys.exit(1)


            cpref = cels[ 0 ]
            gplat = cels[ 1 ]
            datal = {
                'gplat'     : gplat,
                'gtype'     :      cels[ 2 ]  ,
                'gsize'     : int( cels[ 3 ] ),
                'gmin'      : int( cels[ 4 ] ),
                'gmax'      : int( cels[ 5 ] ),
                'file_names':      cels[ 6 ].split(";"),
                'genomesize': GENOME_SIZE,
            }
            datal[ 'max2' ] = ( ( datal['gmax' ] - datal['gsize'] ) * 3 ) + datal['gsize']

            num_files = len(datal['file_names'])

            if num_files == 2:
                for ffname in datal['file_names']:
                    print ffname

                    if not os.path.exists(ffname) or not os.path.isfile(ffname):
                        print "file '%s' does not exists" % ffname
                        sys.exit(1)

            elif  num_files == 1:
                ffname = datal['file_names'][0]
                if not os.path.exists(ffname) or not os.path.isfile(ffname):
                    print "file '%s' does not exists" % ffname
                    sys.exit(1)

            else:
                print "oops2. globed more than 2 files"
                sys.exit(1)

            if gplat not in datas: datas[gplat] = {}
            datas[gplat][cpref] = datal


    for plat in sorted(datas):
        data = datas[plat]
        if   plat == 'illumina':
            datas[plat] = processIllumina(data)
        elif plat == '454'     :
            datas[plat] = process454(data)



    if len( datas ) == 0:
        print "no valid files found. quitting"
        sys.exit(1)

    if len(datas['illumina']) == 0:
        print "no valid illumina files found. quitting"
        sys.exit(1)

    #process454(datas['454'])

    dataf = {}

    for plat in datas:
        for name in datas[plat]:
            if name in dataf:
                print "duplicated name %s. error"  % name
                sys.exit(1)

            dataf[name] = datas[plat][name]

    run_bwa(dataf)


def process454(data):
    for comm_name in sorted(data):
        infile = data['file_names'][0]

        if not infile.endswith('.sff'):
            print 'infile %s is not sff' % infile
            sys.exit(1)

        out_prefix  = os.path.join( MAP_FOLDER , infile  )

        out_fastq   = out_prefix + '.fastq'

        out_fastq_f = out_fastq.replace('.fastq', '_1.fastq')
        out_fastq_r = out_fastq.replace('.fastq', '_2.fastq')
        out_fastq_n = out_fastq.replace('.fastq', '_n.fastq')
        out_fastq_0 = out_fastq.replace('.fastq', '_0.fastq')

        data['file_names'      ] = [out_fastq_f, out_fastq_r]
        data['file_names_454'  ] = infile
        data['file_names_454_p'] = out_prefix
        data['file_names_454_q'] = out_fastq
        data['file_names_454_f'] = out_fastq_f
        data['file_names_454_r'] = out_fastq_r
        data['file_names_454_n'] = out_fastq_n
        data['file_names_454_0'] = out_fastq_0
        data['454_linker'      ] = LINKER

        CMD_EXTRACT = 'sff_extract -c -l %(454_linker)s -Q %(file_names_454)s -o %(file_names_454_p)s'

        CMD_SPLIT   = 'fastqSplitPe.pl %(file_names_454_q)s'

        data['cmds'] = [
            [CMD_EXTRACT % data, 'extracting sff'],
            [CMD_SPLIT   % data, 'splitting sff'  ]
        ]

    data = processIllumina(data)

    return data

def processIllumina(data):
    for comm_name in sorted(data):
        RND_FILE1       = tempfile.mkstemp(dir=TMP_FOLDER, prefix='bwa_', suffix='.bam')
        RND_NAME1       = RND_FILE1[1]
        os.remove(RND_NAME1)

        RND_FILE2       = tempfile.mkstemp(dir=TMP_FOLDER, prefix='bwa_')
        RND_NAME2       = RND_FILE2[1]
        os.remove(RND_NAME2)


        out_sam         = os.path.join( MAP_FOLDER , comm_name + '.sam'      )
        out_bam_tmp     = os.path.join( MAP_FOLDER , comm_name + '.bam'      )

        out_bam_pfx     = os.path.join( OUT_FOLDER , comm_name               )
        out_bam         = os.path.join( OUT_FOLDER , comm_name + '.bam'      )
        out_bai         = out_bam + '.bai'
        out_cov         = os.path.join( OUT_FOLDER , comm_name + '.cov'      )
        out_stats       = os.path.join( OUT_FOLDER , comm_name + '.stats'    )
        out_flag        = os.path.join( OUT_FOLDER , comm_name + '.flagstats')
        out_frc         = os.path.join( OUT_FOLDER , comm_name + '_FRC__CEstats_PE.txt')
        out_frcB        = os.path.join( OUT_FOLDER , comm_name + '_FRC_'     )


        data[comm_name]['bwa_exe'       ] = BWA_EXE
        data[comm_name]['samtools_exe'  ] = SAMTOOLS_EXE
        data[comm_name]['frc_exe'       ] = FRC_EXE

        data[comm_name]['sam'           ] = out_sam
        data[comm_name]['bam_tmp'       ] = out_bam_tmp
        data[comm_name]['bam_tmp_rnd'   ] = RND_NAME1
        data[comm_name]['bam_pfx_rnd'   ] = RND_NAME2
        data[comm_name]['bam_pfx'       ] = out_bam_pfx
        data[comm_name]['bam'           ] = out_bam
        data[comm_name]['bai'           ] = out_bai
        data[comm_name]['cov'           ] = out_cov
        data[comm_name]['stats'         ] = out_stats
        data[comm_name]['flag'          ] = out_flag
        data[comm_name]['frc'           ] = out_frc
        data[comm_name]['frcB'          ] = out_frcB
        filenames = data[comm_name]['file_names']
        data[comm_name]['file_names_str'] = " ".join( filenames )


        data[comm_name]['fastq1'        ] = filenames[0]
        data[comm_name]['sai1'          ] = os.path.join( MAP_FOLDER , os.path.basename( filenames[0] ) + '.sai' )


        if len( filenames ) == 2:
            data[comm_name]['fastq2'        ] = filenames[1]
            data[comm_name]['sai2'          ] = os.path.join( MAP_FOLDER , os.path.basename( filenames[1] ) + '.sai' )

        if 'gtype' in data[ comm_name ] and data[ comm_name ]['gtype'] == 'mp':
            #<(gunzip -c | fastqreversecomp.py )
            fn     = data[comm_name]['fastq1']
            opener = 'cat'

            if fn.endswith('.gz'):
                opener = 'pigz -d -p3 -c'

            data[comm_name]['fastq1'        ]  = "<(%(opener)s %(fn)s | fastx_reverse_complement )" % { 'opener': opener, 'fn': fn }


            if 'fastq2' in data[comm_name]:
                fn     = data[comm_name]['fastq2']
                opener = 'cat'

                if fn.endswith('.gz'):
                    opener = 'pigz -d -p3 -c'
                data[comm_name]['fastq2'        ]  = "<(%(opener)s %(fn)s | fastx_reverse_complement )" % { 'opener': opener, 'fn': fn }

    return data

def run_bwa(data):
    db_name     = os.path.join(DB_FOLDER , CONTIG.replace('/', '_'))
    bam_name    = os.path.join(OUT_FOLDER, CONTIG.replace('/', '_'))

    cmds        = []
    bams        = []
    sum_process = 0



    CMD_BWA_INDEX = "%(bwa_exe)s index -p %(db_name)s -a is %(contig)s" % { 'db_name': db_name,
                                                                            'contig' : CONTIG,
                                                                            'bwa_exe': BWA_EXE}

    bwt_index = db_name + '.bwt'
    if not os.path.exists( bwt_index ):
        cmds.append( [ CMD_BWA_INDEX , 'bwa indexing' ] )


    for comm_name in data:
        info            = data[comm_name       ]
        sam             = info['sam'           ]
        bam_tmp_rnd     = info['bam_tmp_rnd'   ]
        bam_tmp         = info['bam_tmp'       ]
        bam             = info['bam'           ]
        bam_pfx         = info['bam_pfx'       ]
        bai             = info['bai'           ]
        cov             = info['cov'           ]
        stats           = info['stats'         ]
        flag            = info['flag'          ]
        frc             = info['frc'           ]
        frcB            = info['frcB'          ]
        file_names      = info['file_names'    ]
        file_names_str  = info['file_names_str']
        sai1            = info['sai1'          ]
        info['db_name'] = db_name




        if (not os.path.exists( sai1 )) or os.path.getsize( sai1 ) == 0:
            CMD_MAPPER   = "%(bwa_exe)s aln -t 15 %(db_name)s %(fastq1)s > %(sai1)s" % info
            cmds.append([CMD_MAPPER, 'map ' + info['fastq1']])
        else:
            print " SKIPPING ALIGNING %(fastq1)s to %(sai1)s\n" % info


        if 'cmds' in info:
            for cmd in info['cmds']:
                cmds.append(cmd)


        if len(file_names) == 2:
            fastq2         = info['fastq2'        ]
            sai2           = info['sai2'          ]
            if (not os.path.exists( sai2 )) or os.path.getsize( sai2 ) == 0:
                CMD_MAPPER   = "%(bwa_exe)s aln -t 15 %(db_name)s %(fastq2)s > %(sai2)s" % info
                cmds.append([CMD_MAPPER, 'map ' + info['fastq2']])
            else:
                print " SKIPPING ALIGNING %(fastq2)s to %(sai2)s\n" % info

            if (not os.path.exists( sam )) or os.path.getsize( sam ) == 0:
                #TODO: CONVERT TO SAMPE!
                #CMD_ALIGNER  = "bwa samse -f %(sam)s %(db_name)s %(sai)s %(fastq)s " % info
                CMD_ALIGNER  = "%(bwa_exe)s sampe -o 20 -P -f %(sam)s " % info

                if 'gsize' in info:
                    CMD_ALIGNER  += " -a %(gmax)s " % info

                CMD_ALIGNER  = CMD_ALIGNER + "%(db_name)s %(sai1)s %(sai2)s %(fastq1)s %(fastq2)s" % info
                cmds.append([CMD_ALIGNER, 'align ' + info['sam']])
            else:
                print " SKIPPING MAPPING %(fastq1)s and %(fastq2)s, %(sai1)s and %(sai2)s to %(sam)s\n" % info

        else:
            print "oops"
            sys.exit(1)
            if (not os.path.exists( sam )) or os.path.getsize( sam ) == 0:
                #TODO: CONVERT TO SAMPE!
                CMD_ALIGNER  = "%(bwa_exe)s samse -f %(sam)s %(db_name)s %(sai1)s %(fastq1)s " % info
                #CMD_ALIGNER  = "bwa sampe -o 20 -P -f %(sam)s %(db_name)s %(sai)s %(fastq)s " % info
                cmds.append([CMD_ALIGNER, 'align ' + info['sam']])
            else:
                print " SKIPPING MAPPING %(fastq1)s and %(sai1)s to %(sam)s\n" % info



        if (not os.path.exists( bam_tmp )) or os.path.getsize( bam_tmp ) == 0:
            # input in sam, output in bam, min qual 30, min compression
            CMD_CONVERT  = "%(samtools_exe)s view -S -b -q 30 -1 -o %(bam_tmp_rnd)s %(sam)s" % info
            CMD_MV       = "mv %(bam_tmp_rnd)s %(bam_tmp)s" % info
            cmds.append([CMD_CONVERT, 'convert %(sam)s to %(bam_tmp)s'        % info ])
            cmds.append([CMD_MV,      'moving %(bam_tmp_rnd)s to %(bam_tmp)s' % info ])

        else:
            print " SKIPPING CONVERTING %(sam)s to %(bam_tmp)s\n" % info


        if (not os.path.exists( bam ) or (os.path.getsize( bam ) == 0)):
            CMD_SORT     = "%(samtools_exe)s sort -m 53687091200 %(bam_tmp)s %(bam_pfx_rnd)s" % info
            CMD_MV       = "mv %(bam_pfx_rnd)s.bam %(bam)s" % info
            cmds.append([CMD_SORT, 'sort '   + info['bam']])
            cmds.append([CMD_MV  , 'moving ' + info['bam']])
        else:
            print " SKIPPING SORTING %(bam_tmp)s to %(bam)s\n" % info


        if (not os.path.exists( bai ) or (os.path.getsize( bai ) == 0)):
            CMD_INDEX    = '%(samtools_exe)s index %(bam)s'            % info
            cmds.append([CMD_INDEX, 'index ' + bai])
        else:
            print " SKIPPING INDEXING %(bam)s to %(bai)s\n" % info



        if (not os.path.exists( frc )):
            if FRC:
                if 'gsize' in info:
                    #/home/assembly/nobackup/metrics/FRC_align/src/FRC
                    #--pe-sam 120527_I247_FCC0U0NACXX_L6_SZAXPI009264-133.bam
                    #--pe-min-insert 1200 --pe-max-insert 3400 --genome-size 950000000 --output 120527_I247_FCC0U0NACXX_L6_SZAXPI009264-133
                    # _CEstats_PE.txt
                    CMD_FRC = '%(frc_exe)s --%(gtype)s-sam %(bam)s --%(gtype)s-min-insert %(gmin)d --%(gtype)s-max-insert %(gmax)d --genome-size %(genomesize)d --output %(frcB)s' % info
                    cmds.append([CMD_FRC, 'FRC ' , info['frcB']])
                else:
                    print " SKIPPING FRC %(frc)s. no size\n" % info
            else:
                print " SKIPPING FRC %(frc)s. no csv\n" % info
        else:
            print " SKIPPING FRC %(frc)s\n" % info




        if (not os.path.exists( cov ) or (os.path.getsize( cov ) == 0)):
            CMD_COV    = '%(samtools_exe)s depth    %(bam)s   > %(cov)s' % info
            cmds.append([CMD_COV  , 'coverage ' + cov])
        else:
            print " SKIPPING COVERAGE %(bam)s to %(cov)s\n" % info


        if (not os.path.exists( stats ) or (os.path.getsize( stats ) == 0)):
            CMD_STATS  = '%(samtools_exe)s idxstats %(bam)s   > %(stats)s' % info
            cmds.append([CMD_STATS, 'stats ' + stats])
        else:
            print " SKIPPING STATS %(bam)s to %(stats)s\n" % info


        if (not os.path.exists( flag ) or (os.path.getsize( flag ) == 0)):
            CMD_FLAG   = '%(samtools_exe)s flagstat %(bam)s   > %(flag)s' % info
            cmds.append([CMD_FLAG, 'flag ' + flag])
        else:
            print " SKIPPING FLAG %(bam)s to %(flag)s\n" % info


        bams.append( bam )

    #print "\n".join( [str(x) for x in cmds] )







    cmds_count = 0
    for CMD in cmds:
        cmds_count += 1
        run_cmd( CMD[0] , CMD[1] + ' %d/%d' % ( cmds_count, len(cmds) ) )

    if not do_merge:
        print "FINISHED"
        sys.exit(0)








    bam_tmp  = '%s.tmp.bam' % bam_name
    bam_sht  = '%s'         % bam_name
    bam_out  = '%s.bam'     % bam_name


    RND_FILE1 = tempfile.mkstemp(dir=TMP_FOLDER, prefix='bwa_', suffix='.bam')
    RND_FILE2 = tempfile.mkstemp(dir=TMP_FOLDER, prefix='bwa_', suffix='.bam')
    RND_NAME1 = RND_FILE1[1]
    RND_NAME2 = RND_FILE2[1]
    os.remove(RND_NAME1)
    os.remove(RND_NAME2)

    bam_data = {
        'samtools_exe': SAMTOOLS_EXE,
        'bam_tmp'     : bam_tmp,
        'bam'         : bam_out,
        'bamsht'      : bam_sht,
        'bamrnd1'     : RND_NAME1,
        'bamrnd2'     : RND_NAME2,
        'index'       : bam_out + '.bai',
        'cov'         : bam_out + '.cov',
        'stat'        : bam_out + '.stats',
        'flag'        : bam_out + '.flagstats',
        'filter'      : bam_out + '.filtered.bam',
        #'fix'         : bam_out + '.filtered.bam.fixmate.bam',
        #'dedup'       : bam_out + '.filtered.bam.dedup.bam',
        'findex'      : bam_out + '.filtered.bam.bai',
        'fcov'        : bam_out + '.filtered.bam.cov',
        'fstat'       : bam_out + '.filtered.bam.stats',
        'fflag'       : bam_out + '.filtered.bam.flagstats',
        'bams'        : " ".join(bams)
    }

    #print "BAM DATA", bam_data

    # attatch RG tag (source), min compression
    CMD_MERGE  = '%(samtools_exe)s merge -r -1         %(bamrnd1)s %(bams)s'   % bam_data
    #CMD_SORT   = '%(samtools_exe)s sort -m 53687091200 %(bam_tmp)s %(bamsht)s' % bam_data
    CMD_INDEX  = '%(samtools_exe)s index               %(bam)s'                % bam_data
    CMD_COV    = '%(samtools_exe)s depth               %(bam)s   > %(cov)s'    % bam_data
    CMD_STAT   = '%(samtools_exe)s idxstats            %(bam)s   > %(stat)s'   % bam_data
    CMD_FLAG   = '%(samtools_exe)s flagstat            %(bam)s   > %(flag)s'   % bam_data


    #      Flag   Chr                    Description
    #   1 0x0001   p   the read is paired in sequencing
    #   2 0x0002   P   the read is mapped in a proper pair
    #   4 0x0004   u   the query sequence itself is unmapped
    #   8 0x0008   U   the mate is unmapped
    #  16 0x0010   r   strand of the query (1 for reverse)
    #  32 0x0020   R   strand of the mate
    #  64 0x0040   1   the read is the first read in a pair
    # 128 0x0080   2   the read is the second read in a pair
    # 256 0x0100   s   the alignment is not primary
    # 512 0x0200   f   the read fails platform/vendor quality checks
    #1024 0x0400   d   the read is either a PCR or an optical duplicate

    # Filter f 1 (IS paired), f 2 (IS proper pair)  = 3
    #   F 4 (NOT qry is unmapped), F 8 (NOT mate is unpaired),
    #   F 512 (NOT failed QC), F 1024 (NOT PCR duplicate) = 4+8+512+1024= 1548
    # bam format
    CMD_FILTER = '%(samtools_exe)s view -f 1 -F 1548 -b -o %(bamrnd2)s %(bam)s'      % bam_data
    #CMD_FIX    = '%(samtools_exe)s fixmate                 %(filter)s %(fix)s'      % bam_data
    #CMD_DEDUP  = '%(samtools_exe)s rmdup -S                %(filter)s %(dedup)s'    % bam_data

    CMD_FINDEX = '%(samtools_exe)s index                   %(filter)s'               % bam_data
    CMD_FCOV   = '%(samtools_exe)s depth                   %(filter)s > %(fcov)s'    % bam_data
    CMD_FSTAT  = '%(samtools_exe)s idxstats                %(filter)s > %(fstat)s'   % bam_data
    CMD_FFLAG  = '%(samtools_exe)s flagstat                %(filter)s > %(fflag)s'   % bam_data




    #if (not os.path.exists( bam_data['bam_tmp'] )) and (not os.path.exists( bam_data['bam'] )) :
    if (not os.path.exists( bam_data['bam'] )) :
        run_cmd( CMD_MERGE , 'merging'             )

        if os.path.exists( bam_data['bamrnd1'] ) and (os.path.getsize( bam_data['bamrnd1'] ) > 0):
            print "moving tmp %(bamrnd1)s to %(bam)s" % bam_data
            shutil.move( bam_data['bamrnd1'], bam_data['bam'])
        else:
            print "failed creating tmp file %(bamrnd1)s to move it to %(bam)s" % bam_data
            if not dry_run:
                sys.exit(1)
    else:
        print " SKIPPING MERGING %(bams)s to %(bam)s\n" % bam_data


    #if (not os.path.exists( bam_data['bam'    ] )) :
    #    run_cmd( CMD_SORT  , 'sorting'             )
    #else:
    #    print " SKIPPING SORTING %(bam_tmp)s to %(bam)s\n" % bam_data


    if (not os.path.exists( bam_data['index'  ] )) :
        run_cmd( CMD_INDEX , 'indexing'            )
    else:
        print " SKIPPING INDEXING to %(index)s\n" % bam_data

    if (not os.path.exists( bam_data['cov'    ] )) :
        run_cmd( CMD_COV   , 'generating coverage' )
    else:
        print " SKIPPING COVERAGE to %(cov)s\n" % bam_data

    if (not os.path.exists( bam_data['stat'   ] )) :
        run_cmd( CMD_STAT  , 'getting statistics'  )
    else:
        print " SKIPPING STAT to %(stat)s\n" % bam_data

    if (not os.path.exists( bam_data['flag'   ] )) :
        run_cmd( CMD_FLAG  , 'getting flag stats'  )
    else:
        print " SKIPPING FLAG STAT to %(flag)s\n" % bam_data




    #if (not os.path.exists( bam_data['filter' ] )) and (not os.path.exists( bam_data['fix'] ))  and (not os.path.exists( bam_data['dedup'] )):
    if (not os.path.exists( bam_data['filter' ] )) :
        run_cmd( CMD_FILTER, 'filtering'           )

        if os.path.exists( bam_data['bamrnd2'] ) and (os.path.getsize( bam_data['bamrnd2'] ) > 0):
            print "moving tmp %(bamrnd1)s to %(bam)s" % bam_data
            shutil.move( bam_data['bamrnd2'], bam_data['filter'])
        else:
            print "failed creating tmp file %(bamrnd2)s to move it to %(filter)s" % bam_data
            if not dry_run:
                sys.exit(1)

    else:
        print " SKIPPING FILTERING %(bam)s to %(filter)s\n" % bam_data

    #if (not os.path.exists( bam_data['fix'    ] ))  and (not os.path.exists( bam_data['dedup'] )):
    #    run_cmd( CMD_FIX   , 'fixing pair size'    )
    #else:
    #    print " SKIPPING FILTERING %(filter)s to %(fix)s\n" % bam_data

    #if (not os.path.exists( bam_data['dedup'  ] )) :
    #    run_cmd( CMD_DEDUP , 'deduplicating'       )
    #else:
    #    print " SKIPPING FILTERING %(fix)s to %(dedup)s\n" % bam_data


    if (not os.path.exists( bam_data['findex' ] )) :
        run_cmd( CMD_FINDEX, 'final indexing'      )
    else:
        print " SKIPPING INDEXING to %(findex)s\n" % bam_data

    if (not os.path.exists( bam_data['fcov'   ] )) :
        run_cmd( CMD_FCOV  , 'generating coverage' )
    else:
        print " SKIPPING COVERAGE to %(fcov)s\n" % bam_data

    if (not os.path.exists( bam_data['fstat'  ] )) :
        run_cmd( CMD_FSTAT , 'getting statistics'  )
    else:
        print " SKIPPING STAT to %(fstat)s\n" % bam_data

    if (not os.path.exists( bam_data['fflag'  ] )) :
        run_cmd( CMD_FFLAG , 'getting flag stats'  )
    else:
        print " SKIPPING FLAG STAT to %(fflag)s\n" % bam_data


if __name__ == '__main__':
    main()
    print "\n\n"
    print 'finished'
