#!/usr/bin/python
import os, sys
import copy
import subprocess
import multiprocessing
import tempfile
import shutil
import glob
import atexit
from pprint import pprint as pp

#./SConstruct.runReal --incsv=all3.csv --threads=20 -j 5 all

dry_run      = False
#dry_run      = True

do_merge     = True
#do_merge     = False

FRC          = False

LINKER       = '/home/assembly/dev_150/assemblies/mapping/sff_linker.fasta'
LINKER       = '/home/assembly/dev_150/assemblies/mapping/sff_linker.fasta.all'
DB_FOLDER    = 'db'
MAP_FOLDER   = 'map'
OUT_FOLDER   = 'output'
GENOME_SIZE  = 950000000
TMP_FOLDER   = '/run/shm'
TMP_PREFIX   = 'bwa_scons_mapping_'+str(os.getpid())+'_'
FRC_EXE      = '/home/assembly/nobackup/metrics/FRC_align/src/FRC'
BWA_EXE      = '/usr/bin/bwa'
SAMTOOLS_EXE = 'samtools'
USE_PIGZ     = False

num_threads  = 1

ONLY_PROCESS_IF_DONT_EXISTS = True


CSV_PROJECT = None
env         = None

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print "no contig given"
        print sys.argv[0], "<IN PROJECT CSV>"
        sys.exit(1)

    CSV_PROJECT = sys.argv[1]

elif __name__ == 'SCons.Script':
    AddOption('--incsv',
              dest='CSV_PROJECT',
              type='string',
              nargs=1,
              action='store',
              metavar='INCSV',
              help='input csv'
              )

    AddOption('--threads',
              dest='num_threads',
              type='int',
              nargs=1,
              action='store',
              metavar='threads',
              default=0,
              help='number of threads'
              )

    os.environ['SHELL'] = '/bin/bash'
    #env['SHELL'] = os.environ['SHELL']

    env       = Environment( CSV_PROJECT=GetOption('CSV_PROJECT'), ENV = os.environ, SHELL='/bin/bash' )

    CSV_PROJECT = env.GetOption('CSV_PROJECT')

    if CSV_PROJECT is None:
        print "input project csv not defined"
        print "scons --incsv=<IN PROJECT FILE>"
        sys.exit(1)

    print "SHELL"      , os.environ['SHELL']
    print "SCONS SHELL", env['SHELL']



if not os.path.exists(CSV_PROJECT):
    print "input project csv %s does not exists" % CSV_PROJECT
    sys.exit(1)





def my_decider(dependency, target, prev_ni):
    if os.path.exists(str(target)):
        if os.path.getsize(str(target)) > 0:
            return True
        else:
            return False
    else:
        return False
    #print "deciding about target %s: %s" % (str(target), str(res))


##########################
## SETTING UP SCONS
##########################
if __name__ == 'SCons.Script':
    print "running under scons"
    #env = Environment(ENV = os.environ) # Add os.environ, otherwise PATH doesn't get picked up
    #env.SourceCode(".", None)

    env.Decider(my_decider)
    #env.Decider('timestamp-match')
    #env.Decider('timestamp-newer')
    #env.SetOption('max_drift', 1)

    ##########################
    ## GETTING THREADS
    ##########################
    # How many CPU's can we use ?

    #env.SetOption("SHELL", "/bin/bash")
    num_cpu     = multiprocessing.cpu_count()
    num_jobs    = int( env.GetOption('num_jobs'   ) )
    num_threads = int( env.GetOption('num_threads') )
    print "num cpu     %d" % num_cpu
    print "num jobs    %d" % num_jobs
    print "num threads %d" % num_threads

    if num_jobs <= 1:
        num_jobs    = int((num_cpu/8) + 1)

    if num_threads == 0:
        num_threads = (num_jobs*2)

    env.SetOption('num_jobs'   , num_jobs   )

    print "running with %d jobs"    % num_jobs
    print "running with %d threads" % num_threads
    # Set number of simultaneous threads to something less than the number of CPUs
    # /CPUs


def touch(*args, **kwargs):
    targets = None
    sources = None
    lenv    = None
    if 'target' in kwargs:
        targets = kwargs['target']
        sources = kwargs['source']
        lenv    = kwargs['env'   ]
    elif len(args) == 3:
        targets = args[0]
        sources = args[1]
        lenv    = args[2]
    else:
        print "args %s kwargs %s. invalid input to touch" % (str(args), str(kwargs))
        return 1

    sources = [str(s) for s in sources]
    targets = [str(t) for t in targets]

    for target in targets:
        if os.path.exists(target):
            print "  TOUCHING: TARGET %s ALREADY EXISTS. DELETING" % target
            try:
                os.remove(target)
            except:
                print "  TOUCHING: TARGET %s ALREADY EXISTS. ERROR DELETING" % target
                return 1

    for target in targets:
        try:
            print "  TOUCHING: TARGET %s. WRITING" % target
            with open(target, 'w') as tgt:
                tgt.write( str(sources) )
        except:
            print "  TOUCHING: TARGET %s. ERROR WRITING" % target
            return 1
    return 0

def move(*args, **kwargs):
    targets = None
    sources = None
    lenv    = None
    if 'target' in kwargs:
        targets = kwargs['target']
        sources = kwargs['source']
        lenv    = kwargs['env'   ]
    elif len(args) == 3:
        targets = args[0]
        sources = args[1]
        lenv    = args[2]
    else:
        print "args %s kwargs %s. invalid input to touch" % (str(args), str(kwargs))
        return 1

    sources = [str(s) for s in sources]
    targets = [str(t) for t in targets]

    for source in sources:
        if not os.path.exists(source):
            print "  MOVING: SOURCE %s DOES NOT EXISTS. ERROR"   % source
            return 1

    for target in targets:
        if os.path.exists(target):
            print "  MOVING: TARGET %s ALREADY EXISTS. DELETING" % target
            os.remove(target)

    if len(sources) != len(targets):
        print "  MOVING: SOURCE and TARGET LENGHTS DIFFER %d vs %d" % (len(sources), len(targets))
        return 1

    for pos in range(len(sources)):
        source = sources[pos]
        target = targets[pos]

        try:
            shutil.move(source, target)
        except:
            print "  MOVING: FAILED TO MOVE %s to %s" % (source, target)
            return 1

    return 0




##########################
## ADD COMMAND WRAPPER
##########################
#addCommand('bwa indexing', CMD_BWA_INDEX, TARGET = bwt_index, SOURCE = setup['in_fasta'], DEPS = [] })
def addCommand(name, cmd, TARGET=None, SOURCE=[], DATA={}, DEPS=[], REQS=[], PRECIOUS=False, USETMP=False, TMP_SUFFIX=""):
    if TARGET is None:
        print "no target defined:", str(locals())
        sys.exit(1)

    if not isinstance( SOURCE, list ): SOURCE = [ SOURCE ]
    if not isinstance( TARGET, list ): TARGET = [ TARGET ]
    if not isinstance( DEPS  , list ): DEPS   = [ DEPS   ]
    if not isinstance( REQS  , list ): REQS   = [ REQS   ]

    SOURCE = list( set(SOURCE) )
    TARGET = list( set(TARGET) )
    DEPS   = list( set(DEPS  ) )
    REQS   = list( set(REQS  ) )

    if __name__ == 'SCons.Script':
        allOk = True
        for data in [ SOURCE, TARGET, DEPS, REQS ]:
            for val in data:
                if str( type(val) ) == "<class 'SCons.Node.NodeList'>":
                    valStr         = " ".join([str(x) for x in val])
                    valindex       = data.index(val)
                    data[valindex] = valStr
                    print "  CONVERTING FROM %s TO %s" % (val, valStr)


    #print "checking target"
    for fileName in TARGET:
        #print "  checking filename %s" % ( str( fileName ) )

        if not os.path.exists( str(fileName) ):
            print "      TARGET %s DOES NOT EXISTS" % str(fileName)
            allOk = False
        else:
            if os.path.getsize( str(fileName) ) == 0:
                print "      TARGET %s EXISTS BUT IT IS EMPTY" % str(fileName)
                allOk = False

    #print "checking source"
    for fileName in SOURCE:
        #print "  checking filename %s" % ( str( fileName ) )

        if not os.path.exists( str(fileName) ):
            print "      SOURCE %s DOES NOT EXISTS" % str(fileName)
            allOk = False
        else:
            if os.path.getsize( str(fileName) ) == 0:
                print "      SOURCE %s EXISTS BUT IT IS EMPTY" % str(fileName)
                allOk = False

    #print "checking dependences"
    for fileName in DEPS:
        #print "  checking filename %s" % ( str( fileName ) )

        if not os.path.exists( str(fileName) ):
            print "      DEPENDENCY %s DOES NOT EXISTS" % str(fileName)
            allOk = False
        else:
            if os.path.getsize( str(fileName) ) == 0:
                print "      DEPENDENCY %s EXISTS BUT IT IS EMPTY" % str(fileName)
                allOk = False

    #print "checking requirements"
    for fileName in REQS:
        #print "  checking filename %s" % ( str( fileName ) )

        if not os.path.exists( str(fileName) ):
            print "      REQUIREMENT %s DOES NOT EXISTS" % str(fileName)
            allOk = False
        else:
            if os.path.getsize( str(fileName) ) == 0:
                print "      REQUIREMENT %s EXISTS BUT IT IS EMPTY" % str(fileName)
                allOk = False



    pdata  = { 'SOURCE': SOURCE, 'TARGET': TARGET, 'RESULTS': TARGET, 'DEPS': DEPS, 'REQS': REQS }

    datan  = copy.copy(DATA)
    for key in pdata:
        datan[key] = pdata[key]




    outFile = None
    tmpFile = None
    if (not allOk) and USETMP:
        tmpFile         = tempfile.mktemp(dir=TMP_FOLDER, prefix=TMP_PREFIX, suffix=TMP_SUFFIX)
        print "      USING TMP :: %s as %s" % ( datan['TARGET'][0], tmpFile )
        datan['TARGET'] = tmpFile



    cmdStr = str(cmd) % datan

    print "      CMD STRING", cmdStr
    print "       ", "\n        ".join(["%s: %s"%(x, pdata[x]) for x in sorted( pdata ) ])

    cmdCall = cmdStr
    if hasattr(cmd, '__call__'): #if functions, send function instead of string
        cmdCall = cmd



    if __name__   == '__main__':
        print "      running"
        if hasattr(cmdCall, '__call__'):
            cmdCall(datan['TARGET'], datan['SOURCE'])
        else:
            run_cmd(cmdStr, name)
        if USETMP:
            shutil.move(datan['TARGET'], outFile)

    elif __name__ == 'SCons.Script':
        print "      adding"

        if ONLY_PROCESS_IF_DONT_EXISTS:
            if allOk:
                print "        all ok. skipping"
                #res = env.Command(TARGET, SOURCE, cmdStr)

                env.Ignore( datan['TARGET'], datan['SOURCE'] )

                for dp in DEPS: env.Ignore( datan['TARGET'], dp )
                for rq in REQS: env.Ignore( datan['TARGET'], rq )

            else:
                print "        not ok. adding"
                env.Command(datan['TARGET'], datan['SOURCE'], cmdCall)

        else:
            print "        appending always"
            env.Command(datan['TARGET'], datan['SOURCE'], cmdCall)


        if (not allOk) and USETMP:
            print "        USING TMP: %s to %s" % (datan['TARGET'], datan['RESULTS'][0])
            env.Command( datan['RESULTS'][0] , datan['TARGET'],    move       )

        for dp in DEPS: env.Depends(  datan['TARGET'], dp )
        for rq in REQS: env.Requires( datan['TARGET'], rq )


        if PRECIOUS:   env.Precious( datan['RESULTS'] )


        print "\n\n"
        return datan['RESULTS']

    else:
        print "not running as main neither as scons"
        sys.exit(1)




##########################
## STAND ALONE RUNNER
##########################
def run_cmd( CMD , name ):
    if not dry_run:
        print "  running %s :: %s" % ( name , CMD )
        #ret = subprocess.call(CMD, shell=True, executable='/bin/bash')
        #if ret != 0:
        #    if ret < 0:
        #        print "error running %s. killed by signal %d\n" % (name, ret)
        #        sys.exit(ret)
        #    else:
        #        print "error running %s. error #%d\n" % (name, ret)
        #        sys.exit(ret)
        #else:
        #    print "    running %s finished successfully\n" % ( name )
        #
        #return ret
    else:
        print "  DRY RUN:: skipping %s :: %s\n" % ( name , CMD )
        return 0

def main():
    setup = []
    with open(CSV_PROJECT, 'r') as csvfhd:
        for line in csvfhd:
            line = line.strip()

            if len(line) == 0: continue
            if line[0]   == "#": continue

            in_fasta, in_csv = line.split("\t")

            if not os.path.exists(in_fasta):
                print "input fasta %s does not exists" % in_fasta
                sys.exit(1)

            if not os.path.exists(in_csv):
                print "input csv %s does not exists" % in_csv
                sys.exit(1)


            base_folder = os.path.dirname( os.path.abspath(in_csv) )
            proj_name   = os.path.basename(base_folder)
            db_folder   = os.path.join(base_folder, DB_FOLDER  )
            map_folder  = os.path.join(base_folder, MAP_FOLDER )
            out_folder  = os.path.join(base_folder, OUT_FOLDER )

            if 'all' not in COMMAND_LINE_TARGETS:
                if proj_name not in COMMAND_LINE_TARGETS:
                    print "project %s not to be built. skipping parsing" % proj_name
                    continue

            print "PROJECT NAME: %s" % proj_name
            print "FASTA FILE  : %s" % in_fasta
            print "CSV   FILE  : %s" % in_csv
            print "BASE  FOLDER: %s" % base_folder
            print "DB    FOLDER: %s" % db_folder
            print "MAP   FOLDER: %s" % map_folder
            print "OUT   FOLDER: %s" % out_folder
            print

            if not dry_run:
                if not os.path.exists(db_folder):
                    os.makedirs(db_folder)

                if not os.path.exists(map_folder):
                    os.makedirs(map_folder)

                if not os.path.exists(out_folder):
                    os.makedirs(out_folder)

            projsetup = {
                    'proj_name'  : proj_name,
                    'in_fasta'   : in_fasta,
                    'in_csv'     : in_csv,
                    'base_folder': base_folder,
                    'db_folder'  : db_folder,
                    'map_folder' : map_folder,
                    'out_folder' : out_folder
                }
            pp(projsetup)
            setup.append(projsetup)

    projout = []
    for projsetup in setup:
        projout.extend( project(projsetup) )

    projout = [ x for x in sorted(list(set(projout))) if x is not None ]

    if __name__ == 'SCons.Script':

        env.Alias( 'all', projout )

    print "target ALL:", "\n            ".join( projout )
    for projsetup in setup:
        print "target %s: %s" % (projsetup['proj_name'].upper(), projsetup['outfile'])
    print "\n\n\n"


def project( setup ):
    datas   = {}

    ##########################
    ## READ CSV
    ##########################
    with open(setup['in_csv'], 'r') as fhd:
        line_count = 0
        for line in fhd:
            line_count += 1
            if line_count == 1:
                continue

            line = line.strip()
            if len(line) == 0 or line[0] == '#':
                continue

            #0
            #file_name                                             , group_name, library_name
            #120527_I247_FCC0U0NACXX_L7_TOMxitDGJDWAAPEI-93_*.fq.gz, MP_2000_1 , jumps_MP_2000

            #120624_I232_FCC0U3HACXX_L5_TOMxitDGJDWAAPEI-93,mp,2000,1800,2200,/home/assembly/tomato150/denovo/arcanum/raw/illumina/matepair_2000/120624_I232_FCC0U3HACXX_L5_TOMxitDGJDWAAPEI-93_2.fq.gz;/home/assembly/tomato150/denovo/arcanum/raw/illumina/matepair_2000/120624_I232_FCC0U3HACXX_L5_TOMxitDGJDWAAPEI-93_1.fq.gz

            cels   = line.split('\t')
            print  "parsing line '%s'" % str(cels)
            if len(cels) != 7:
                print "wrong number of columns"
                sys.exit(1)


            cpref = cels[ 0 ]
            gplat = cels[ 1 ]
            datal = {
                'gplat'     :      gplat  ,
                'gtype'     :      cels[ 2 ]  ,
                'gsize'     : int( cels[ 3 ] ),
                'gmin'      : int( cels[ 4 ] ),
                'gmax'      : int( cels[ 5 ] ),
                'file_names':      cels[ 6 ].split(";"),
                'genomesize': GENOME_SIZE,
            }
            datal[ 'max2' ] = ( ( datal['gmax' ] - datal['gsize'] ) * 3 ) + datal['gsize']

            num_files = len(datal['file_names'])


            if   num_files == 1:
                ffname = datal['file_names'][0]
                if not os.path.exists(ffname) or not os.path.isfile(ffname):
                    print "file '%s' does not exists" % ffname
                    sys.exit(1)

            elif num_files == 2:
                for ffname in datal['file_names']:
                    print ffname

                    if not os.path.exists(ffname) or not os.path.isfile(ffname):
                        print "file '%s' does not exists" % ffname
                        sys.exit(1)

            else:
                print "oops2. globed more than 2 files"
                sys.exit(1)

            if gplat not in datas: datas[gplat] = {}
            datas[gplat][cpref] = datal



    for plat in sorted(datas):
        data = datas[plat]
        if   plat == 'illumina':
            datas[plat] = processIllumina(data, setup)
        elif plat == '454'     :
            datas[plat] = process454(data, setup)



    if len( datas ) == 0:
        print "no valid files found. quitting"
        sys.exit(1)

    if len(datas['illumina']) == 0:
        print "no valid illumina files found. quitting"
        sys.exit(1)


    dataf = {}

    for plat in datas:
        for name in datas[plat]:
            if name in dataf:
                print "duplicated name %s. error"  % name
                sys.exit(1)

            dataf[name] = datas[plat][name]

    finalout = run_bwa(dataf, setup)
    return finalout


def process454(data, setup):
    for comm_name in sorted(data):
        print "parsing 454 :: %s" % comm_name

        infile = data[comm_name]['file_names'][0]

        print "parsing 454 :: %s :: %s" % ( comm_name, infile )

        if not infile.endswith('.sff'):
            print 'infile %s is not sff' % infile
            sys.exit(1)


        out_prefix  = os.path.join( setup['map_folder'] , comm_name  )
        print "parsing 454 :: %s :: %s :: out prefix %s" % ( comm_name, infile, out_prefix )

        out_fastq   = out_prefix + '.fastq'

        out_fastq_0 = out_fastq.replace('.fastq', '_0.fastq')
        out_fastq_f = out_fastq.replace('.fastq', '_1.fastq')
        out_fastq_r = out_fastq.replace('.fastq', '_2.fastq')
        out_fastq_n = out_fastq.replace('.fastq', '_n.fastq')

        data[comm_name]['file_names'      ] = [out_fastq_f, out_fastq_r]
        data[comm_name]['file_names_454'  ] = infile
        data[comm_name]['file_names_454_p'] = out_prefix
        data[comm_name]['file_names_454_q'] = out_fastq
        data[comm_name]['file_names_454_f'] = out_fastq_f
        data[comm_name]['file_names_454_r'] = out_fastq_r
        data[comm_name]['file_names_454_n'] = out_fastq_n
        data[comm_name]['file_names_454_0'] = out_fastq_0
        data[comm_name]['454_linker'      ] = LINKER

        CMD_EXTRACT = 'sff_extract -c -l %(454_linker)s -Q %(file_names_454)s -o %(file_names_454_p)s'

        CMD_SPLIT   = 'fastqSplitPe.pl %(file_names_454_q)s'

        addCommand('extracting sff', CMD_EXTRACT, TARGET = data[comm_name]['file_names_454_q'], SOURCE = data[comm_name]['file_names_454'  ], DATA = data[comm_name], REQS = [ ])
        addCommand('splitting sff' , CMD_SPLIT  , TARGET = data[comm_name]['file_names'      ], SOURCE = data[comm_name]['file_names_454_q'], DATA = data[comm_name], REQS = [ ])

    data = processIllumina(data, setup)

    return data

def processIllumina(data, setup):
    ##########################
    ## GENERATE FILE NAMES
    ##########################
    for comm_name in sorted(data):
        RND_NAME1       = tempfile.mktemp(dir=TMP_FOLDER, prefix='bwa_', suffix='.bam')
        RND_NAME2       = tempfile.mktemp(dir=TMP_FOLDER, prefix='bwa_')


        out_sam         = os.path.join( setup['map_folder'] , comm_name + '.sam'      )
        out_bam_tmp     = os.path.join( setup['map_folder'] , comm_name + '.bam'      )

        out_bam_pfx     = os.path.join( setup['out_folder'] , comm_name               )
        out_bam         = os.path.join( setup['out_folder'] , comm_name + '.bam'      )
        out_bai         = out_bam + '.bai'
        out_cov         = os.path.join( setup['out_folder'] , comm_name + '.cov'      )
        out_stats       = os.path.join( setup['out_folder'] , comm_name + '.stats'    )
        out_flag        = os.path.join( setup['out_folder'] , comm_name + '.flagstats')
        out_ok          = os.path.join( setup['out_folder'] , comm_name + '.ok'       )
        out_frc         = os.path.join( setup['out_folder'] , comm_name + '_FRC__CEstats_PE.txt')
        out_frcB        = os.path.join( setup['out_folder'] , comm_name + '_FRC_'     )


        filenames = data[comm_name]['file_names']
        if len( filenames ) != 2:
            sys.exit( 1 )

        data[comm_name]['bwa_exe'        ] = BWA_EXE
        data[comm_name]['samtools_exe'   ] = SAMTOOLS_EXE
        data[comm_name]['frc_exe'        ] = FRC_EXE
        data[comm_name]['threads'        ] = num_threads

        data[comm_name]['sam'            ] = out_sam
        data[comm_name]['bam_tmp'        ] = out_bam_tmp
        data[comm_name]['bam_tmp_rnd'    ] = RND_NAME1
        data[comm_name]['bam_pfx_rnd'    ] = RND_NAME2
        data[comm_name]['bam_pfx_rnd_bam'] = RND_NAME2 + '.bam'
        data[comm_name]['bam_pfx'        ] = out_bam_pfx
        data[comm_name]['bam'            ] = out_bam
        data[comm_name]['bai'            ] = out_bai
        data[comm_name]['cov'            ] = out_cov
        data[comm_name]['stats'          ] = out_stats
        data[comm_name]['flag'           ] = out_flag
        data[comm_name]['ok'             ] = out_ok
        data[comm_name]['frc'            ] = out_frc
        data[comm_name]['frcB'           ] = out_frcB
        data[comm_name]['file_names_str' ] = " ".join( filenames )

        data[comm_name]['fastq1'         ] = filenames[0]
        data[comm_name]['sai1'           ] = os.path.join( setup['map_folder'] , os.path.basename( filenames[0] ) + '.sai' )
        data[comm_name]['fastq1cmd'      ] = data[comm_name]['fastq1']

        data[comm_name]['fastq2'         ] = filenames[1]
        data[comm_name]['sai2'           ] = os.path.join( setup['map_folder'] , os.path.basename( filenames[1] ) + '.sai' )
        data[comm_name]['fastq2cmd'      ] = data[comm_name]['fastq2']



        if 'gtype' in data[ comm_name ] and data[ comm_name ]['gtype'] == 'mp':
            if data[ comm_name ]['gplat'] == 'illumina':
                #<(gunzip -c | fastqreversecomp.py )
                fn     = data[comm_name]['fastq1']
                opener = 'cat'

                if fn.endswith('.gz'):
                    opener = 'gunzip -c'
                    if USE_PIGZ:
                        opener = 'pigz -d -p3 -c'

                data[comm_name]['fastq1cmd'        ]  = "<(%(opener)s %(fn)s | fastx_reverse_complement )" % { 'opener': opener, 'fn': fn }


                if 'fastq2' in data[comm_name]:
                    fn     = data[comm_name]['fastq2']
                    opener = 'cat'

                    if fn.endswith('.gz'):
                        opener = 'gunzip -c'

                        if USE_PIGZ:
                            opener = 'pigz -d -p3 -c'

                    data[comm_name]['fastq2cmd'        ]  = "<(%(opener)s %(fn)s | fastx_reverse_complement )" % { 'opener': opener, 'fn': fn }

    return data


##########################
## GENERATE COMMANDS
##########################
def run_bwa(data, setup):
    in_fasta_bn   = os.path.basename( setup['in_fasta'] ).replace('/', '_')
    db_name       = os.path.join( setup['db_folder' ], in_fasta_bn )
    bam_name      = os.path.join( setup['out_folder'], in_fasta_bn )
    pre_ok        = os.path.join( setup['out_folder'], in_fasta_bn + '.ok' )

    bwt_index     = db_name + '.bwt'


    cmds          = []
    bams          = []
    deps          = []
    oks           = []
    sum_process   = 0

    index_data    = { 'db_name': db_name, 'contig' : setup['in_fasta'], 'bwa_exe': BWA_EXE}
    CMD_BWA_INDEX = "%(bwa_exe)s index -p %(db_name)s -a is %(contig)s"
    addCommand( 'bwa indexing', CMD_BWA_INDEX, TARGET=bwt_index, SOURCE=setup['in_fasta'], DATA=index_data, DEPS=[], PRECIOUS=True )


    ##########################
    ## ADD COMMANDS TO EACH INPUT FILE
    ##########################
    for comm_name in data:
        info              = data[comm_name       ]
        info['bwt_index'] = bwt_index
        info['db_name'  ] = db_name

        #CMD_ALIGNER  = "%(bwa_exe)s samse -f %(sam)s %(db_name)s %(sai1)s %(fastq1cmd)s "
        #addCommand('align', CMD_ALIGNER, TARGET = info['sam'], SOURCE = info['fastq1'], DEPS = [bwt_index, info['sai1']] })

        CMD_MAPPER1  = "%(bwa_exe)s aln -t %(threads)d %(db_name)s %(fastq1cmd)s > %(TARGET)s"
        CMD_MAPPER2  = "%(bwa_exe)s aln -t %(threads)d %(db_name)s %(fastq2cmd)s > %(TARGET)s"

        CMD_ALIGNER  = "%(bwa_exe)s sampe -o 20 -P "
        #CMD_ALIGNER  = "%(bwa_exe)s sampe -o 20 -P -f %(TARGET)s "

        if 'gsize' in info:
            CMD_ALIGNER  += " -a %(gmax)s "
        CMD_ALIGNER += "%(db_name)s %(sai1)s %(sai2)s %(fastq1cmd)s %(fastq2cmd)s"

        CMD_ALIGNER += " | %(samtools_exe)s view -Su -b -q 30 -1 -"
        #CMD_CONVERT  = "%(samtools_exe)s view -S -b -q 30 -1 %(sam)s > %(TARGET)s" # TARGET = bam_tmp
        #CMD_MV1      = "mv %(bam_tmp_rnd)s %(bam_tmp)s"

        CMD_ALIGNER += " | %(samtools_exe)s sort -m 53687091200 -o -l 1 -@ 20 -m 2G - %(TARGET)s > %(TARGET)s"
        #CMD_SORT     = "%(samtools_exe)s sort -m 53687091200 -o -l 1 -@ 20 -m 2G %(bam_tmp)s %(TARGET)s > %(TARGET)s"
        #samtools sort -m 53687091200 -o -l 1 -@ 20 -m 2G 136.bam /run/shm/bwa_scons_mapping_13117_J9DFw5 > /run/shm/bwa_scons_mapping_13117_J9DFw5
        #CMD_MV2      = "mv %(bam_pfx_rnd_bam)s %(bam)s"

        CMD_INDEX    = '%(samtools_exe)s index    %(bam)s'
        CMD_COV      = '%(samtools_exe)s depth    %(bam)s   > %(TARGET)s'
        CMD_STATS    = '%(samtools_exe)s idxstats %(bam)s   > %(TARGET)s'
        CMD_FLAG     = '%(samtools_exe)s flagstat %(bam)s   > %(TARGET)s'
        CMD_OK       = touch


        addCommand('map'             , CMD_MAPPER1, TARGET = info['sai1']              , SOURCE = info['fastq1']                                           , DATA = info, REQS = [ info['bwt_index'] ]                    , USETMP=True )
        addCommand('map'             , CMD_MAPPER2, TARGET = info['sai2']              , SOURCE = info['fastq2']                                           , DATA = info, REQS = [ info['bwt_index'] ]                    , USETMP=True )
        #addCommand('align'           , CMD_ALIGNER, TARGET = info['sam']               , SOURCE = [ info['fastq1'], info['fastq2'] ]                       , DATA = info, DEPS = [ bwt_index, info['sai1'], info['sai2'] ], USETMP=True )
        addCommand('align'           , CMD_ALIGNER, TARGET = info['bam']               , SOURCE = [ info['fastq1'], info['fastq2'] ]                       , DATA = info, DEPS = [ bwt_index, info['sai1'], info['sai2'] ], USETMP=True )


        CMD_CONVERT_tmp = False
        if (not os.path.exists(info['bam'])) or ( os.path.getsize(info['bam']) == 0 ):
            CMD_CONVERT_tmp = True
        #addCommand('convert'         , CMD_CONVERT, TARGET = info['bam_tmp']           , SOURCE = info['sam'        ]                                      , DATA = info                                                   , USETMP=True )
        #addCommand('moving'         , CMD_MV1    , TARGET = info['bam_tmp'    ]       , SOURCE = info['bam_tmp_rnd']                                      , DATA = info)


        #CMD_SORT_tmp = False
        #if (not os.path.exists(info['bam'])) or ( os.path.getsize(info['bam']) == 0 ):
        #    CMD_SORT_tmp = True
        #addCommand('sort'            , CMD_SORT   , TARGET = info['bam']                , SOURCE = info['bam_tmp'    ]                                      , DATA = info                                                  , USETMP=True)
        #addCommand('moving'          , move       , TARGET = info['bam']               , SOURCE = info['bam_pfx_rnd_bam']                                  , DATA = info)

        addCommand('index'           , CMD_INDEX  , TARGET = info['bai']               , SOURCE = info['bam']                                              , DATA = info                                                                )
        addCommand('coverage'        , CMD_COV    , TARGET = info['cov']               , SOURCE = info['bam']                                              , DATA = info, DEPS = [ info['bai'] ]                           , USETMP=True)
        addCommand('stats'           , CMD_STATS  , TARGET = info['stats']             , SOURCE = info['bam']                                              , DATA = info, DEPS = [ info['bai'] ]                           , USETMP=True)
        addCommand('flag'            , CMD_FLAG   , TARGET = info['flag']              , SOURCE = info['bam']                                              , DATA = info, DEPS = [ info['bai'] ]                           , USETMP=True)

        deps = [ info['bai'], info['cov'], info['stats'], info['flag'] ]

        if FRC:
            if 'gsize' in info:
                #/home/assembly/nobackup/metrics/FRC_align/src/FRC
                #--pe-sam 120527_I247_FCC0U0NACXX_L6_SZAXPI009264-133.bam
                #--pe-min-insert 1200 --pe-max-insert 3400 --genome-size 950000000 --output 120527_I247_FCC0U0NACXX_L6_SZAXPI009264-133
                # _CEstats_PE.txt
                CMD_FRC = '%(frc_exe)s --%(gtype)s-sam %(bam)s --%(gtype)s-min-insert %(gmin)d --%(gtype)s-max-insert %(gmax)d --genome-size %(genomesize)d --output %(frcB)s'
                addCommand('FRC', CMD_FRC, TARGET = info['frc'], SOURCE = info['bam'], DATA = info, DEPS = [ info['bai'] ] )
                deps.append( info['frc'] )

            else:
                print " SKIPPING FRC %(frc)s. no size\n" % info

        else:
            print " SKIPPING FRC %(frc)s. disabled\n" % info

        addCommand('oking'           , CMD_OK     , TARGET = info['ok']                , SOURCE = deps, DATA = info )

        oks.append(info['ok'])

        bams.append( info['bam'] )

    CMD_PRE_OK = touch
    addCommand('preoking'            , CMD_PRE_OK , TARGET = pre_ok                , SOURCE = oks, DATA = data )


    RND_NAME1 = None
    RND_NAME2 = None
    bam_tmp   = '%s.tmp.bam' % bam_name
    bam_sht   = '%s'         % bam_name
    bam_out   = '%s.bam'     % bam_name

    if ( not do_merge ) or ( len(bams) == 1 ):
        bam_name  = bams[0]
        RND_NAME1 = ""
        RND_NAME2 = ""
        bam_tmp   = '%s.tmp.bam' % bam_name
        bam_sht   = '%s'         % bam_name
        bam_out   = '%s'         % bam_name

    else:
        RND_NAME1 = tempfile.mktemp(dir=TMP_FOLDER, prefix='bwa_', suffix='.bam')
        RND_NAME2 = tempfile.mktemp(dir=TMP_FOLDER, prefix='bwa_', suffix='.bam')
        bam_tmp   = '%s.tmp.bam' % bam_name
        bam_sht   = '%s'         % bam_name
        bam_out   = '%s.bam'     % bam_name





    bam_data = {
        'samtools_exe': SAMTOOLS_EXE,
        'sam_filter'  : '-f 1 -F 1548',
        'bam_tmp'     : bam_tmp,
        'bam'         : bam_out,
        'bamsht'      : bam_sht,
        'bamrnd1'     : RND_NAME1,
        'bamrnd2'     : RND_NAME2,
        #'bamrnd1bam'  : RND_NAME1 + '.bam',
        #'bamrnd2bam'  : RND_NAME2 + '.bam',
        'index'       : bam_out   + '.bai',
        'cov'         : bam_out   + '.cov',
        'stat'        : bam_out   + '.stats',
        'bok'         : bam_out   + '.ok',
        'flag'        : bam_out   + '.flagstats',

        'filter'      : bam_out   + '.filtered.bam',
        #'fix'         : bam_out   + '.filtered.bam.fixmate.bam',
        #'dedup'       : bam_out   + '.filtered.bam.dedup.bam',
        'findex'      : bam_out   + '.filtered.bam.bai',
        'fcov'        : bam_out   + '.filtered.bam.cov',
        'fstat'       : bam_out   + '.filtered.bam.stats',
        'fflag'       : bam_out   + '.filtered.bam.flagstats',
        'fok'         : bam_out   + '.filtered.bam.ok',

        'dedup'       : bam_out   + '.filtered.bam.dedup.bam',
        #'fix'         : bam_out   + '.filtered.bam.fixmate.bam',
        #'dedup'       : bam_out   + '.filtered.bam.dedup.bam',
        'dindex'      : bam_out   + '.filtered.bam.dedup.bam.bai',
        'dcov'        : bam_out   + '.filtered.bam.dedup.bam.cov',
        'dstat'       : bam_out   + '.filtered.bam.dedup.bam.stats',
        'dflag'       : bam_out   + '.filtered.bam.dedup.bam.flagstats',
        'dok'         : bam_out   + '.filtered.bam.dedup.bam.ok',

        'bams'        : " ".join(bams)
    }


        #      Flag   Chr                    Description
        #   1 0x0001   p   the read is paired in sequencing
        #   2 0x0002   P   the read is mapped in a proper pair
        #   4 0x0004   u   the query sequence itself is unmapped
        #   8 0x0008   U   the mate is unmapped
        #  16 0x0010   r   strand of the query (1 for reverse)
        #  32 0x0020   R   strand of the mate
        #  64 0x0040   1   the read is the first read in a pair
        # 128 0x0080   2   the read is the second read in a pair
        # 256 0x0100   s   the alignment is not primary
        # 512 0x0200   f   the read fails platform/vendor quality checks
        #1024 0x0400   d   the read is either a PCR or an optical duplicate

        # Filter f 1 (IS paired), f 2 (IS proper pair)  = 3
        #   F 4 (NOT qry is unmapped), F 8 (NOT mate is unpaired),
        #   F 512 (NOT failed QC), F 1024 (NOT PCR duplicate) = 4+8+512+1024= 1548
        # bam format



    ##########################
    ## IF NOT TO MERGE, STOPS HERE
    ##########################
    setup['data'] = data
    if ( not do_merge ) or ( len(bams) == 1 ):
        bam_data['bok'] = info['ok']

        filterbam(bam_data)

        print "FINISHED", setup['proj_name']
        env.Alias( setup['proj_name'], bam_data['dok'] )
        setup['bamdata'] = bam_data
        setup['outfile'] = bam_data['dok']
        return [ bam_data['dok'] ]



    ##########################
    ## MERGE BAMS
    ##########################
    # attatch RG tag (source), min compression
    CMD_MERGE  = '%(samtools_exe)s merge -r -1         %(TARGET)s %(bams)s'
    #CMD_MV1    = "mv %(bamrnd1)s %(bam)s"
    CMD_INDEX  = '%(samtools_exe)s index               %(bam)s'
    CMD_COV    = '%(samtools_exe)s depth               %(bam)s    > %(TARGET)s'
    CMD_STAT   = '%(samtools_exe)s idxstats            %(bam)s    > %(TARGET)s'
    CMD_FLAG   = '%(samtools_exe)s flagstat            %(bam)s    > %(TARGET)s'
    CMD_BOK    = touch


    CMD_MERGE_tmp = False
    if (not os.path.exists( bam_data['bam'] )) or ( os.path.getsize(bam_data['bam']) == 0 ):
        CMD_MERGE_tmp = True

    addCommand('merging'        , CMD_MERGE , TARGET = bam_data['bam']    , SOURCE = bams               , DATA = bam_data, DEPS = [ pre_ok ]            , USETMP=CMD_MERGE_tmp )
    #addCommand('moving'        , CMD_MV1   , TARGET = bam_data['bam']    , SOURCE = bam_data['bamrnd1'], DATA = bam_data                              )


    addCommand('indexing'       , CMD_INDEX , TARGET = bam_data['index']  , SOURCE = bam_data['bam']    , DATA = bam_data                                             )
    addCommand('coverage'       , CMD_COV   , TARGET = bam_data['cov']    , SOURCE = bam_data['bam']    , DATA = bam_data, DEPS = [ bam_data['index']  ], USETMP=True )
    addCommand('statistics'     , CMD_STAT  , TARGET = bam_data['stat']   , SOURCE = bam_data['bam']    , DATA = bam_data, DEPS = [ bam_data['index']  ], USETMP=True )
    addCommand('flag stats'     , CMD_FLAG  , TARGET = bam_data['flag']   , SOURCE = bam_data['bam']    , DATA = bam_data, DEPS = [ bam_data['index']  ], USETMP=True )
    bdeps = [ bam_data['index'], bam_data['cov'], bam_data['stat'], bam_data['flag'] ]
    addCommand('bam ok'         , CMD_BOK   , TARGET = bam_data['bok']    , SOURCE = bdeps                                                               )

    filterbam(bam_data)

    if __name__ == 'SCons.Script':
        print "FINISHED", setup['proj_name']
        env.Alias( setup['proj_name'], bam_data['dok'] )
        setup['bamdata'] = bam_data
        setup['outfile'] = bam_data['dok']
        return [ bam_data['dok'] ]

def filterbam( bam_data ):
    CMD_FILTER = '%(samtools_exe)s view %(sam_filter)s -b %(bam)s > %(TARGET)s'
    #CMD_MV2    = 'mv %(bamrnd2)s %(filter)s'
    #CMD_FIX    = '%(samtools_exe)s fixmate             %(filter)s %(fix)s'
    #CMD_DEDUP  = '%(samtools_exe)s rmdup -S            %(filter)s %(dedup)s'
    CMD_FINDEX = '%(samtools_exe)s index               %(filter)s'
    CMD_FCOV   = '%(samtools_exe)s depth               %(filter)s > %(TARGET)s'
    CMD_FSTAT  = '%(samtools_exe)s idxstats            %(filter)s > %(TARGET)s'
    CMD_FFLAG  = '%(samtools_exe)s flagstat            %(filter)s > %(TARGET)s'
    CMD_FOK    = touch

    CMD_FILTER_tmp = False
    if ( not os.path.exists( bam_data['filter'] )) or ( os.path.getsize(bam_data['filter']) == 0 ):
        CMD_FILTER_tmp = True

    addCommand('filtering'      , CMD_FILTER, TARGET = bam_data['filter'] , SOURCE = bam_data['bam']    , DATA = bam_data, DEPS = [ bam_data['bok']    ], USETMP=CMD_FILTER_tmp )
    #addCommand('moving'        , CMD_MV2   , TARGET = bam_data['filter'] , SOURCE = bam_data['bamrnd2'], DATA = bam_data                                )


    addCommand('Findexing'      , CMD_FINDEX, TARGET = bam_data['findex'] , SOURCE = bam_data['filter'] , DATA = bam_data                                             )
    addCommand('Fcoverage'      , CMD_FCOV  , TARGET = bam_data['fcov']   , SOURCE = bam_data['filter'] , DATA = bam_data, DEPS = [ bam_data['findex'] ], USETMP=True )
    addCommand('Fstatistics'    , CMD_FSTAT , TARGET = bam_data['fstat']  , SOURCE = bam_data['filter'] , DATA = bam_data, DEPS = [ bam_data['findex'] ], USETMP=True )
    addCommand('Fflag stats'    , CMD_FFLAG , TARGET = bam_data['fflag']  , SOURCE = bam_data['filter'] , DATA = bam_data, DEPS = [ bam_data['findex'] ], USETMP=True )
    addCommand('Fok'            , CMD_FOK   , TARGET = bam_data['fok']    , SOURCE = [
                                                                                    bam_data['findex' ],
                                                                                    bam_data['fcov'   ],
                                                                                    bam_data['fstat'  ],
                                                                                    bam_data['fflag'  ],
                                                                                    bam_data['bok']
                                                                                    ]                   , DATA = bam_data                                )


    CMD_DEDUP  = '%(samtools_exe)s rmdup               %(filter)s   %(TARGET)s'
    #CMD_MV2    = 'mv %(bamrnd2)s %(filter)s'
    #CMD_FIX    = '%(samtools_exe)s fixmate             %(filter)s %(fix)s'
    #CMD_DEDUP  = '%(samtools_exe)s rmdup -S            %(filter)s %(dedup)s'
    CMD_DINDEX = '%(samtools_exe)s index               %(filter)s'
    CMD_DCOV   = '%(samtools_exe)s depth               %(filter)s > %(TARGET)s'
    CMD_DSTAT  = '%(samtools_exe)s idxstats            %(filter)s > %(TARGET)s'
    CMD_DFLAG  = '%(samtools_exe)s flagstat            %(filter)s > %(TARGET)s'
    CMD_DOK    = touch

    CMD_DEDUP_tmp = False
    if ( not os.path.exists( bam_data['dedup'] )) or ( os.path.getsize(bam_data['dedup']) == 0 ):
        CMD_DEDUP_tmp = True

    addCommand('deduping'       , CMD_DEDUP , TARGET = bam_data['dedup']  , SOURCE = bam_data['filter'] , DATA = bam_data, DEPS = [ bam_data['fok']    ], USETMP=CMD_DEDUP_tmp )
    #addCommand('moving'        , CMD_MV2   , TARGET = bam_data['filter'] , SOURCE = bam_data['bamrnd2'], DATA = bam_data                                )


    addCommand('Dindexing'      , CMD_DINDEX, TARGET = bam_data['dindex'] , SOURCE = bam_data['dedup']  , DATA = bam_data                                             )
    addCommand('Dcoverage'      , CMD_DCOV  , TARGET = bam_data['dcov']   , SOURCE = bam_data['dedup']  , DATA = bam_data, DEPS = [ bam_data['dindex'] ], USETMP=True )
    addCommand('Dstatistics'    , CMD_DSTAT , TARGET = bam_data['dstat']  , SOURCE = bam_data['dedup']  , DATA = bam_data, DEPS = [ bam_data['dindex'] ], USETMP=True )
    addCommand('Dflag stats'    , CMD_DFLAG , TARGET = bam_data['dflag']  , SOURCE = bam_data['dedup']  , DATA = bam_data, DEPS = [ bam_data['dindex'] ], USETMP=True )
    addCommand('Dok'            , CMD_DOK   , TARGET = bam_data['dok']    , SOURCE = [
                                                                                    bam_data['dindex' ],
                                                                                    bam_data['dcov'   ],
                                                                                    bam_data['dstat'  ],
                                                                                    bam_data['dflag'  ],
                                                                                    bam_data['fok']
                                                                                    ]                   , DATA = bam_data                                )


def bf_to_str(bf):
    """Convert an element of GetBuildFailures() to a string
    in a useful way."""
    import SCons.Errors
    if bf is None: # unknown targets product None in list
        return '(unknown tgt)'
    elif isinstance(bf, SCons.Errors.StopError):
        return str(bf)
    elif bf.node:
        return str(bf.node) + ': ' + bf.errstr                              + "\nEXECUTOR: " + str(bf.executor) + "\nACTION: " + str(bf.action)
    elif bf.filename:
        return bf.filename  + ': ' + bf.errstr + "\nCOMMAND: " + bf.command + "\nEXECUTOR: " + str(bf.executor) + "\nACTION: " + str(bf.action)

    return 'unknown failure: ' + bf.errstr


def build_status():
    """Convert the build status to a 2-tuple, (status, msg)."""
    from SCons.Script import GetBuildFailures
    bf = GetBuildFailures()
    if bf:
        # bf is normally a list of build failures; if an element is None,
        # it's because of a target that scons doesn't know anything about.
        status = 'failed'
        failures_message = "\n".join(["Failed building %s" % bf_to_str(x)
                           for x in bf if x is not None])
    else:
        # if bf is None, the build completed successfully.
        status = 'ok'
        failures_message = ''
    return (status, failures_message)


def leave():
    cleantmp()
    display_build_status()

def cleantmp():
    for tmpfile in glob.glob('TMP_PREFIX'):
        if os.path.exists(tmpfile) and os.path.isfile(tmpfile):
            try:
                print "removing tmp file %s" % tmpfile
                os.remove(tmpfile)
                print "  sucess"
            except:
                print "failed to remoge tmp file %s" % tmpfile

def display_build_status():
    """Display the build status.  Called by atexit.
    Here you could do all kinds of complicated things."""
    status, failures_message = build_status()
    if status == 'failed':
       print "FAILED!!!!"  # could display alert, ring bell, etc.
    elif status == 'ok':
       print "Build succeeded."
    print failures_message





if __name__ == '__main__':
    main()
    print "\n\n"
    print 'finished'

elif __name__ == 'SCons.Script':
    main()

    env.Default( env.Alias('all') )

    #print "alias all:", ", ".join( [str(x) for x in env.Alias('all') ] )



    atexit.register(leave)
